import React, { Component } from 'react';
import Searchbar from './Searchbar/Searchbar.jsx';

class HeaderComponent extends Component {
  render() {
    return(
      <div id="header">
        <div id="logo"></div>
        <Searchbar />
        <div id="tools">
        	<div className="tool-icon" id="user"></div>
        	<div className="tool-icon" id="settings"></div>
        	<div className="tool-icon" id="notifications"></div>
        </div>
      </div>
    )
  }
}

export default HeaderComponent;